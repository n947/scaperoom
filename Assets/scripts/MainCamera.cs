﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public GameObject Player;
    public Vector2 Minimo;
    public Vector2 Maximo;
    public float Smoth;
    Vector2 velocity;

    private void FixedUpdate()
    {
        //mediante estas funciones por medio de mathf calcularemos las posiciones en x e y dependediendo de
        //la posicion de nuestro player y al final se le multiplicara por el Smoth que es lo que tardara la camara
        //en seguirnos
        float posX = Mathf.SmoothDamp(transform.position.x, Player.transform.position.x, ref velocity.x, Smoth);
        float posY = Mathf.SmoothDamp(transform.position.y, Player.transform.position.y, ref velocity.y, Smoth);

        //al final le pasaremos todo esto a la camara en esta funcion para que nos pueda seguir como objetivo
        //que con el metodo Clamp sirve para delimitar los espacios que dimos anteriormente
        transform.position = new Vector3(Mathf.Clamp(posX, Minimo.x, Maximo.x), Mathf.Clamp(posY, Minimo.y, Maximo.y), transform.position.z);
    }

}
