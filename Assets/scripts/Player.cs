﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //variables para moverme izquierda derecha y saltar
    public float Spedd;
    public float JumpForce;
    private float Horizontal;

    //llamamos al animador para las animaciones
    private Animator Animator;

    private Rigidbody2D Rigidbody2D;

    private bool Grounded;
    
    void Start()
    {
        //llamando al rigidbody del personaje
        Rigidbody2D = GetComponent<Rigidbody2D>();
        //llamando al animator del personaje
        Animator = GetComponent<Animator>();
    }


    void Update()
    {
        //llamando la fuerza horizontal que mueve al persona
        Horizontal = Input.GetAxisRaw("Horizontal");
        if (Horizontal < 0.0f)
        {
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }
        else if (Horizontal > 0.0f) { 
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f); 
        }

        //llamamos en el animator el boleano que es Running para que pase de una animacion a otra
        //agregando Horizontal que es con la variable que adquiere movimiento y sea diferente de 0
        Animator.SetBool("Running", Horizontal != 0.0f);
        
        //un metodo que llama a un raycast para verificar si el personaje choca con el suelo para no volver a saltar
        Debug.DrawRay(transform.position, Vector3.down * 0.2f, Color.red);
        if (Physics2D.Raycast(transform.position, Vector3.down, 0.2f))
        {
            Grounded = true;
        }
        else { 
            Grounded = false; 
        }


        //haciendo que el salto funcione llamandolo desde un metodo y confirmando que este tocando el suelo
        if (Input.GetKeyDown(KeyCode.W) && Grounded)
        {
            Jump();
        }
    }
    private void Jump()
    {
        // ultiplicado por JumForce para regular la fuerza de salto
        Rigidbody2D.AddForce(Vector2.up * JumpForce);
    }
    private void FixedUpdate()
    {
        Rigidbody2D.velocity = new Vector2(Horizontal, Rigidbody2D.velocity.y);
    }
}
