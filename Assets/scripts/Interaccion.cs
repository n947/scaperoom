﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaccion : MonoBehaviour
{
	
	public bool grabbed;
	RaycastHit2D hit;
	public float distance = 0.2f;
	public Transform holdpoint;
	public float throwforce;
	public LayerMask notgrabbed;

	
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		//precionaremos espacio para tomar el objeto
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//si esta siendo sostenido un item
			if (!grabbed)
			{
				//dibujamos un raycast y determinamos si esa distancia dle raycast esta tocando un item y a que distancia

				hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distance);

				if (hit.collider != null && hit.collider.tag == "Item")
				{
					grabbed = true;

				}


				//ahora verificamos si soltamos el item 
			}
			else if (!Physics2D.OverlapPoint(holdpoint.position,notgrabbed))
			{
				grabbed = false;

				//con esta funcion decimos si soltamos el objeto el objeto se le agregara una fuerza para soltarla
				if (hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
				{

					hit.collider.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x, 1) * throwforce;
				}


				
			}


		}
		//en esta pequeña parte acomodamos el item en nuetro gamobject vacio y toma su posicion
		if (grabbed)
			hit.collider.gameObject.transform.position = holdpoint.position;


	}

	//dibujamos un raycast tomando en cuenta la posicion de nuestro player ya sea que este volteando a la derecha o izquierda
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;

		Gizmos.DrawLine(transform.position, transform.position + Vector3.right * transform.localScale.x * distance);
	}
}
