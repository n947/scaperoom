﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warp : MonoBehaviour
{
    //llamamos a los warps
    public GameObject target;

    //un metodo para cuando cargue el juego
    private void Awake()
    {
        //hacemos invisibles los sprites cuando empieze el juego 
        //Y usamos el GetChild para volverlo tambien invisible
        GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
    }

    //estamos haciendo un metodo para cuando colicionesmos con los warps este cambie de un lugar a otro
    private void OnTriggerEnter2D(Collider2D other)
    {
        //verificamos si lo que choca tiene el tag de Player
        if (other.tag == "Player")
        {
            //aqui se efectua el cambio de posiciones del target que es jugador con el warp
            //el GetChild es para referirnos al objeto hijo
            other.transform.position = target.transform.GetChild(0).transform.position;
        }
    }
}
